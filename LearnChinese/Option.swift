//
//  Option.swift
//  EngRead
//
//  Created by Ernie Cho on 6/18/17.
//  Copyright © 2017 ErnShu. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class OptionUI: UIViewController {
    
    //Variabes for IU Properties.
    @IBOutlet weak var varVolume: UISlider!
    @IBOutlet weak var varpitchMultiplier: UISlider!
    @IBOutlet weak var varvoiceRate: UISlider!
    @IBOutlet weak var LearnLevel: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        varVolume.value = self.LoadOption().volume
        varpitchMultiplier.value = self.LoadOption().multi
        varvoiceRate.value = self.LoadOption().rate
        LearnLevel.selectedSegmentIndex = self.LoadOption().level
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //Action Button
    @IBAction func applysettingsButton(_ sender: AnyObject) {
        let alert = UIAlertController(title: "Option changes will take affect.", message: "New settings will be applied immediatly to application.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        self.SaveOption(volume: varVolume.value, multi: varpitchMultiplier.value, rate: varvoiceRate.value, level: LearnLevel.selectedSegmentIndex)
    }
    @IBAction func ResetOption(_ sender: UIButton) {
        let alert = UIAlertController(title: "Reset changes.", message: "Resetting your application options to default.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        varVolume.value = 1
        varpitchMultiplier.value = 1
        varvoiceRate.value = 0.5
        LearnLevel.selectedSegmentIndex = 0
        self.SaveOption(volume: varVolume.value, multi: varpitchMultiplier.value, rate: varvoiceRate.value, level: LearnLevel.selectedSegmentIndex)
    }
    
    func SaveOption(volume: Float, multi: Float, rate: Float, level: Int) {
        UserDefaults.standard.set(volume, forKey: "volumeOption")
        UserDefaults.standard.set(multi, forKey: "multiOption")
        UserDefaults.standard.set(rate, forKey: "rateOption")
        UserDefaults.standard.set(level, forKey: "levelOption")
        UserDefaults.standard.set(true, forKey: "SavedOption")
        UserDefaults.standard.synchronize()
    }
    
    func LoadOption() -> (volume: Float, multi: Float, rate: Float, level: Int) {
        var valuevolume: Float
        var valuemulti: Float
        var valuerate: Float
        var valuelevel: Int
        let Saved = UserDefaults.standard.bool(forKey: "SavedOption")
        if Saved {
            valuevolume = UserDefaults.standard.float(forKey: "volumeOption")
            valuemulti = UserDefaults.standard.float(forKey: "multiOption")
            valuerate = UserDefaults.standard.float(forKey: "rateOption")
            valuelevel = UserDefaults.standard.integer(forKey: "levelOption")
        } else {
            valuevolume = 1
            valuemulti = 1
            valuerate = 0.5
            valuelevel = 0
        }
        return (valuevolume, valuemulti, valuerate, valuelevel)
    }
}
