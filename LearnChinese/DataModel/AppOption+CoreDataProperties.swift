//
//  AppOption+CoreDataProperties.swift
//  LearnChinese
//
//  Created by Ernie Cho on 11/24/17.
//  Copyright © 2017 ErnShu. All rights reserved.
//
//

import Foundation
import CoreData


extension AppOption {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AppOption> {
        return NSFetchRequest<AppOption>(entityName: "AppOption")
    }

    @NSManaged public var level: Int16
    @NSManaged public var pitchmultiplier: Float
    @NSManaged public var voicerate: Float
    @NSManaged public var volume: Float

}
