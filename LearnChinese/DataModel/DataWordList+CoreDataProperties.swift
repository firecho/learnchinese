//
//  DataWordList+CoreDataProperties.swift
//  LearnChinese
//
//  Created by Ernie Cho on 11/24/17.
//  Copyright © 2017 ErnShu. All rights reserved.
//
//

import Foundation
import CoreData


extension DataWordList {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DataWordList> {
        return NSFetchRequest<DataWordList>(entityName: "DataWordList")
    }

    @NSManaged public var definition: String?
    @NSManaged public var wordname: String?

}
