//
//  LearnWordsUI.swift
//  EngRead
//
//  Created by Ernie Cho on 6/23/17.
//  Copyright © 2017 ErnShu. All rights reserved.
//

import UIKit
import AVFoundation
import Speech

class LearnWordsUI: UIViewController, SFSpeechRecognizerDelegate {
    //varibles for play word button.
    var speechSynthesizerLW = AVSpeechSynthesizer()
    //var utterWordLW = AVSpeechUtterance()
    //variables for buttons and labels
    @IBOutlet weak var indexcountLabel: UILabel!
    @IBOutlet weak var indexField: UITextField!
    @IBOutlet weak var wordTypeLabel: UILabel!
    @IBOutlet weak var definLabel: UILabel!
    @IBOutlet weak var sentenceLabel: UILabel!
    @IBOutlet weak var wordbuttonVar: UIButton!
    @IBOutlet weak var micbuttonVar: UIButton!
    @IBOutlet weak var yourSpokenVar: UILabel!
    //Dicate variable.
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "zh-CN"))!
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private var audioEngine = AVAudioEngine()
    
    //Message Array
    let PostiveWordsData = ["Great Job","Excelent","You Learning Alot","Your Improving Alot","Job Well Done","Doing Great"]
    let PostiveBodyData = ["I encourage you to continue learning chinese. Your getting better at pronouncing the word correctly. Try each day to pronounce each word. You soon be able to talk chinese better just keep at it.","You are improving so much. Pretty soon you be far superior to the native speaker of chinese. You have answer the word correctly. I encourage you to continue learning chinese.","You doing great and improving a lot in chinese. Job well done in improving your chinese. I encourage you to continue learning chinese.","Your getting good at pronouncing chinese words. The more you learn stronger you will become. You will far surpass most native chinese speaker. I encourage you to continue learning chinese.","You come a long way from not know much to speaking very well. I believe in you to succeed and better yourself.  I encourage you to continue learning chinese."]
    let WrongWordsData = ["Nice Try", "Wrong Word", "Not Correct","Keep on Trying","Don't Let This Stop You","Wrong"]
    let WrongBodyData = ["That's the wrong word. Lookup the word and see how it's answer and don't give up","Don't give up. You can do it.","That's not the right word.","That's the wrong","Study more and don't give up.","Wrong word. Don't give up. You can do it","Maybe, you should lookup the word and see how it's pronounce.","Find word and check how it's pronounce."]
    
    //Array Index
    var WordNameCount: Int = 0
    
    //Learn Engine Counter
    var LearnCounter: Int = 0
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        //Load save data
        self.LoadData()
        //Load the button with a a word.
        self.wordbuttonVar.setTitle(DataArrayManager.WordNamesVal[WordNameCount] , for: .normal)
        // Disable the record buttons until authorization has been granted.
        micbuttonVar.isEnabled = false
        // load array count number.
        indexcountLabel.text = String(DataArrayManager.WordNamesVal.count / 2)
        indexField.text = String((self.WordNameCount / 2) + 1)
        definLabel.text = String(DataArrayManager.WordNamesVal[WordNameCount + 1])
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        speechRecognizer.delegate = self
        
        SFSpeechRecognizer.requestAuthorization { authStatus in
            /*
             The callback may not be called on the main thread. Add an
             operation to the main queue to update the record button's state.
             */
            OperationQueue.main.addOperation {
                switch authStatus {
                case .authorized:
                    self.micbuttonVar.isEnabled = true
                    
                case .denied:
                    self.micbuttonVar.isEnabled = false
                    self.micbuttonVar.setTitle("User denied access to speech recognition", for: .disabled)
                    
                case .restricted:
                    self.micbuttonVar.isEnabled = false
                    self.micbuttonVar.setTitle("Speech recognition restricted on this device", for: .disabled)
                    
                case .notDetermined:
                    self.micbuttonVar.isEnabled = false
                    self.micbuttonVar.setTitle("Speech recognition not yet authorized", for: .disabled)
                }
            }
        }
    }
    
    
    
    private func startRecording() throws {
        
        // Cancel the previous task if it's running.
        if let recognitionTask = recognitionTask {
            recognitionTask.cancel()
            self.recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()
        try audioSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
        try audioSession.setMode(AVAudioSessionModeMeasurement)
        try audioSession.setActive(true, with: .notifyOthersOnDeactivation)
        
        self.recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        let inputNode = audioEngine.inputNode
        let recognitionRequest = self.recognitionRequest
        
        // Configure request so that results are returned before audio recording is finished
        recognitionRequest!.shouldReportPartialResults = true
        
        // A recognition task represents a speech recognition session.
        // We keep a reference to the task so that it can be cancelled.
        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest!) { result, error in
            var isFinal = false
            
            if let result = result {
                self.yourSpokenVar.text = result.bestTranscription.formattedString
                isFinal = result.isFinal
            }
            
            if error != nil || isFinal {
                self.audioEngine.stop()
               inputNode.removeTap(onBus: 0)
                
                self.recognitionRequest = nil
                self.recognitionTask = nil
                
                //point to edit add a decision condition if statment.
                if self.wordbuttonVar.currentTitle == self.yourSpokenVar.text! && isFinal {
                    //Using simple array if correct answer was given changes the word to next word in array.
                    if self.WordNameCount < DataArrayManager.WordNamesVal.count && self.WordNameCount >= 0 {
                        self.LearnEngine();
                        self.wordbuttonVar.setTitle(DataArrayManager.WordNamesVal[self.WordNameCount] , for: .normal)
                        self.yourSpokenVar.text = nil
                        self.indexField.text = String((self.WordNameCount / 2 ) + 1)
                        self.definLabel.text = String(DataArrayManager.WordNamesVal[self.WordNameCount + 1])
                        self.SaveData();
                    }
                } else {
                    //alert that sends a wrong message.
                    let randomWrongWord = arc4random() % UInt32(self.WrongWordsData.count)
                    let randomWrongBody = arc4random() % UInt32(self.WrongBodyData.count)
                    self.createAlert(title: self.WrongWordsData[Int(randomWrongWord)], message: self.WrongBodyData[Int(randomWrongBody)])
                    print("Your answer below is the wrong answer.")
                    print(self.yourSpokenVar.text!)
                    print("Below is the answer looking for.")
                    print(self.wordbuttonVar.currentTitle!)
                }
                
                self.micbuttonVar.isEnabled = true
                self.micbuttonVar.setTitle("Start Recording", for: [])
            }
        }
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer: AVAudioPCMBuffer, when: AVAudioTime) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()
        
        try audioEngine.start()
        
        yourSpokenVar.text = "(Go ahead, I'm listening)"
    }
    
    // MARK: SFSpeechRecognizerDelegate
    
    public func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            micbuttonVar.isEnabled = true
            micbuttonVar.setTitle("Start Recording", for: [])
        } else {
            micbuttonVar.isEnabled = false
            micbuttonVar.setTitle("Recognition not available", for: .disabled)
        }
    }
    
    
    // MARK: Interface Builder actions
    

    @IBAction func wordButton(_ sender: UIButton) {
            //read the text button.
            let utterWordLW = AVSpeechUtterance(string: wordbuttonVar.currentTitle!)
            //Change the voice.
            utterWordLW.voice = AVSpeechSynthesisVoice(language: "zh-CN")
            //speak word.
            utterWordLW.rate = OptionUI().LoadOption().rate
            utterWordLW.volume = OptionUI().LoadOption().volume
            utterWordLW.pitchMultiplier = OptionUI().LoadOption().multi
            speechSynthesizerLW.speak(utterWordLW)
            print("Word Play Button is Pressed")
    }
    
    @IBAction func micanswerButton(_ sender: AnyObject) {
        if (audioEngine.isRunning) {
            audioEngine.stop()
            recognitionRequest?.endAudio()
            micbuttonVar.isEnabled = false
            micbuttonVar.setTitle("Stopping", for: .disabled)
        } else {
            try! startRecording()
            micbuttonVar.setTitle("Stop Recording", for: [])
        }
    }
    
    func createAlert (title:String, message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in alert.dismiss(animated: true, completion: nil); print ("Message Alert UI Was Activated") }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func skipButton(_ sender: UIButton) {
        //Using simple array if correct answer was given changes the word to next word in array.
        if self.WordNameCount < DataArrayManager.WordNamesVal.count && self.WordNameCount >= 0 {
            self.WordNameCount += 2
            self.SaveData()
        } else {
            self.WordNameCount = 0
        }
        self.wordbuttonVar.setTitle(DataArrayManager.WordNamesVal[self.WordNameCount] , for: .normal)
        definLabel.text = String(DataArrayManager.WordNamesVal[WordNameCount + 1])
        indexField.text = String((self.WordNameCount / 2) + 1)
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        //Using simple array if correct answer was given changes back the word in array.
        if self.WordNameCount < DataArrayManager.WordNamesVal.count && self.WordNameCount >= 0 {
            self.WordNameCount -= 2
            self.SaveData()
            } else {
            self.WordNameCount = 0
        }
        self.wordbuttonVar.setTitle(DataArrayManager.WordNamesVal[self.WordNameCount] , for: .normal)
        definLabel.text = String(DataArrayManager.WordNamesVal[WordNameCount + 1])
        indexField.text = String((self.WordNameCount / 2) + 1)
    }
    
    @IBAction func tapoutKeyboard(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    @IBAction func editedindexNum(_ sender: UITextField) {
        if self.WordNameCount >= 0 && self.WordNameCount < DataArrayManager.WordNamesVal.count {
            self.WordNameCount = Int(indexField.text!)!
            self.wordbuttonVar.setTitle(DataArrayManager.WordNamesVal[self.WordNameCount] , for: .normal)
        } else {
            WordNameCount = 0
        }
    }
    
    func LoadData() {
        if self.WordNameCount < DataArrayManager.WordNamesVal.count && self.WordNameCount >= 0 {
        WordNameCount = UserDefaults.standard.integer(forKey: "learnwordsuiWNC")
        } else {
        WordNameCount = 0
        }
    }
    
    func SaveData() {
        //Save Point when application shutdown.
        if self.WordNameCount < DataArrayManager.WordNamesVal.count && self.WordNameCount >= 0 {
        UserDefaults.standard.set(WordNameCount, forKey: "learnwordsuiWNC")
        UserDefaults.standard.synchronize()
        } else {
        WordNameCount = 0
        }
    }
    
    func LearnEngine() {
        let randomPostiveWord = arc4random() % UInt32(self.PostiveWordsData.count)
        let randomPostiveBody = arc4random() % UInt32(self.PostiveBodyData.count)
        switch OptionUI().LoadOption().level {
        case 0:
            //Easy
            //alert that sends a correct message.
            self.createAlert(title: self.PostiveWordsData[Int(randomPostiveWord)], message: self.PostiveBodyData[Int(randomPostiveBody)])
            // Correct add a counter
            self.WordNameCount += 2
        case 1:
            //Hard
            if self.LearnCounter <= 2 {
                self.createAlert(title: "Your answer is correct" , message: "How many do you have left \(3 - self.LearnCounter) then we can move onto the next lesson.");
                self.LearnCounter += 1;
            } else {
                self.createAlert(title: self.PostiveWordsData[Int(randomPostiveWord)], message: self.PostiveBodyData[Int(randomPostiveBody)])
                // Correct add a counter
                self.WordNameCount += 2;
                self.LearnCounter = 0;
            }
        case 2:
            //Hardcore
            if self.LearnCounter <= 4 {
                self.createAlert(title: "Your answer is correct" , message: "How many do you have left \(5 - self.LearnCounter) then we can move onto the next lesson.");
                self.LearnCounter += 1;
            } else {
                self.createAlert(title: self.PostiveWordsData[Int(randomPostiveWord)], message: self.PostiveBodyData[Int(randomPostiveBody)])
                // Correct add a counter
                self.WordNameCount += 2;
                self.LearnCounter = 0;
            }
        default:
            //Easy
            //alert that sends a correct message.
            self.createAlert(title: self.PostiveWordsData[Int(randomPostiveWord)], message: self.PostiveBodyData[Int(randomPostiveBody)])
            // Correct add a counter
            self.WordNameCount += 2
        }
    }
}
