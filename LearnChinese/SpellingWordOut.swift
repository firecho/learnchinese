//
//  SpellingWordOut.swift
//  EngRead
//
//  Created by Ernie Cho on 9/11/17.
//  Copyright © 2017 ErnShu. All rights reserved.
//

import AVFoundation
import UIKit

class SpellingOut: UIViewController {
    //Spelling UI input and output.
    @IBOutlet weak var WordDisplay: UILabel!
    @IBOutlet weak var spellinginputTextfield: UITextField!
    @IBOutlet weak var definLabel: UILabel!
    @IBOutlet weak var WordCountLabel: UILabel!
    @IBOutlet weak var ChineseCharacterLabel: UILabel!
    
    //Message Array
    let PostiveWordsData = ["Great Job","Excelent","You Learning Alot","Your Improving Alot","Job Well Done","Doing Great"]
    let PostiveBodyData = ["I encourage you to continue learning chinese. Your getting better at writing the word correctly. Try each day to spelling words. You soon be able to write chinese better just keep at it.","You are improving so much. Pretty soon you be far superior to the native speaker of chinese. You have answer the word correctly. I encourage you to continue learning chinese.","You doing great and improving a lot in chinese. Job well done in improving your chinese. I encourage you to continue learning chinese.","Your getting good at writing chinese words. The more you learn stronger you will become. You will far surpass most native chinese speaker. I encourage you to continue learning chinese.","You come a long way from not know much to speaking very well. I believe in you to succeed and better yourself.  I encourage you to continue learning chinese."]
    let WrongWordsData = ["Nice Try", "Wrong Word", "Not Correct","Keep on Trying","Don't Let This Stop You","Wrong"]
    let WrongBodyData = ["That's the wrong word. Lookup the word and see how it's answer and don't give up","Don't give up. You can do it.","That's not the right word.","That's the wrong","Study more and don't give up.","Wrong word. Don't give up. You can do it","Maybe, you should lookup the word and see how it's spelled.","Find word and check how it's spelled."]
    
    //Array Index
    var WordNameCount: Int = 0
    
    //varibles for play word button.
    var speechSynthesizerSP = AVSpeechSynthesizer()
    
    //Learn Engine Counter
    var LearnCounter: Int = 0
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        //Loading Save Data.
        self.LoadData()
        WordCountLabel.text = String(DataArrayManager.WordNamesVal.count / 2)
        WordDisplay.text = String((self.WordNameCount / 2) + 1)
        ChineseCharacterLabel.text = String(DataArrayManager.WordNamesVal[WordNameCount])
        definLabel.text = String(DataArrayManager.WordNamesVal[WordNameCount + 1])
    }
    
    func createAlert (title:String, message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in alert.dismiss(animated: true, completion: nil); print ("Message Alert UI Was Activated") }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func PlayWordbtn(_ sender: UIButton) {
        //read the text button.
        let utterWordSP = AVSpeechUtterance(string: DataArrayManager.WordNamesVal[WordNameCount])
        //Change the voice.
        utterWordSP.voice = AVSpeechSynthesisVoice(language: "zh-CN")
        //speak word.
        utterWordSP.rate = OptionUI().LoadOption().rate
        utterWordSP.volume = OptionUI().LoadOption().volume
        utterWordSP.pitchMultiplier = OptionUI().LoadOption().multi
        speechSynthesizerSP.speak(utterWordSP)
        print("Word Play Button is Pressed")
    }
    @IBAction func Okbtn(_ sender: UIButton) {
        //point to edit add a decision condition if statment.
        if spellinginputTextfield.text == DataArrayManager.WordNamesVal[WordNameCount] {
            //Using simple array if correct answer was given changes the word to next word in array.
            if self.WordNameCount < DataArrayManager.WordNamesVal.count && self.WordNameCount >= 0 {
                self.LearnEngine();
                spellinginputTextfield.text = nil
                self.SaveData();
            }
        } else {
            //alert that sends a wrong message.
            let randomWrongWord = arc4random() % UInt32(self.WrongWordsData.count)
            let randomWrongBody = arc4random() % UInt32(self.WrongBodyData.count)
            self.createAlert(title: self.WrongWordsData[Int(randomWrongWord)], message: self.WrongBodyData[Int(randomWrongBody)])
            print("user answer is wrong.")
            spellinginputTextfield.text = nil
        }
        //update word and display.
        WordDisplay.text = String((self.WordNameCount / 2) + 1)
        ChineseCharacterLabel.text = String(DataArrayManager.WordNamesVal[WordNameCount])
        definLabel.text = String(DataArrayManager.WordNamesVal[WordNameCount + 1])
    }
    
    @IBAction func tapoutKeyboard(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    func LoadData() {
        if self.WordNameCount < DataArrayManager.WordNamesVal.count && self.WordNameCount >= 0 {
            WordNameCount = UserDefaults.standard.integer(forKey: "spellingwordoutWNC")
        } else {
            WordNameCount = 0
        }
    }
    
    func SaveData() {
        //Save Point when application shutdown.
        if self.WordNameCount < DataArrayManager.WordNamesVal.count && self.WordNameCount >= 0 {
            UserDefaults.standard.set(WordNameCount, forKey: "spellingwordoutWNC")
            UserDefaults.standard.synchronize()
        } else {
            WordNameCount = 0
        }
    }
    
    func LearnEngine() {
        let randomPostiveWord = arc4random() % UInt32(self.PostiveWordsData.count)
        let randomPostiveBody = arc4random() % UInt32(self.PostiveBodyData.count)
        switch OptionUI().LoadOption().level {
        case 0:
            //Easy
            //alert that sends a correct message.
            self.createAlert(title: self.PostiveWordsData[Int(randomPostiveWord)], message: self.PostiveBodyData[Int(randomPostiveBody)])
            // Correct add a counter
            self.WordNameCount += 2
        case 1:
            //Hard
            if self.LearnCounter <= 2 {
                self.createAlert(title: "Your answer is correct" , message: "How many do you have left \(3 - self.LearnCounter) then we can move onto the next lesson.");
                self.LearnCounter += 1;
            } else {
                self.createAlert(title: self.PostiveWordsData[Int(randomPostiveWord)], message: self.PostiveBodyData[Int(randomPostiveBody)])
                // Correct add a counter
                self.WordNameCount += 2;
                self.LearnCounter = 0;
            }
        case 2:
            //Hardcore
            if self.LearnCounter <= 4 {
                self.createAlert(title: "Your answer is correct" , message: "How many do you have left \(5 - self.LearnCounter) then we can move onto the next lesson.");
                self.LearnCounter += 1;
            } else {
                self.createAlert(title: self.PostiveWordsData[Int(randomPostiveWord)], message: self.PostiveBodyData[Int(randomPostiveBody)])
                // Correct add a counter
                self.WordNameCount += 2;
                self.LearnCounter = 0;
            }
        default:
            //Easy
            //alert that sends a correct message.
            self.createAlert(title: self.PostiveWordsData[Int(randomPostiveWord)], message: self.PostiveBodyData[Int(randomPostiveBody)])
            // Correct add a counter
            self.WordNameCount += 2
        }
    }
}
