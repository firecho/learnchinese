//
//  ViewController.swift
//  EngRead
//
//  Created by Ernie Cho on 4/10/17.
//  Copyright © 2017 ErnShu. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    //Properties and Objects
    @IBOutlet weak var textfieldView: UITextView!
    var speechSynthesizer = AVSpeechSynthesizer()
    var utterWord = AVSpeechUtterance()

    override func viewDidLoad() {
        super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func playButton(_ sender: AnyObject) {
    if !speechSynthesizer.isSpeaking || !speechSynthesizer.isPaused {
        //read the textbox.
        utterWord = AVSpeechUtterance(string: textfieldView.text)
        //Change the voice.
        utterWord.voice = AVSpeechSynthesisVoice(language: "zh-CN")
        //speak word.
        utterWord.rate = OptionUI().LoadOption().rate
        utterWord.volume = OptionUI().LoadOption().volume
        utterWord.pitchMultiplier = OptionUI().LoadOption().multi
        speechSynthesizer.speak(utterWord)
    }
    else{
        speechSynthesizer.continueSpeaking()
        }
    } //end playButton func

    
    @IBAction func pauseButton(_ sender: AnyObject) {
        if !speechSynthesizer.isPaused {
        speechSynthesizer.pauseSpeaking(at: AVSpeechBoundary.immediate)
        }
        else{
            speechSynthesizer.continueSpeaking()
        }
    }
    
    @IBAction func stopButton(_ sender: AnyObject) {
        speechSynthesizer.stopSpeaking(at: AVSpeechBoundary.immediate)
    }
    
    @IBAction func clearButton(_ sender: UIButton) {
        textfieldView.text = ""
    }
    
    
}
